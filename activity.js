db.users.insertMany([
		{
			"firstName": "Diane",
			"lastName": "Murphy",
			"email": "dmurphy@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Mary",
			"lastName": "Pattterson",
			"email": "mpatterson@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Jeff",
			"lastName": "Firrelli",
			"email": "jfirrelli@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Gerard",
			"lastName": "Bondur",
			"email": "gbondur@mail.com",
			"isAdmin": false,
			"isActive": true
		},
		{
			"firstName": "Pamela",
			"lastName": "Castillio",
			"email": "pcastillio@mail.com",
			"isAdmin": true,
			"isActive": false
		},
		{
			"firstName": "George",
			"lastName": "Vanauf",
			"email": "gvanauf@mail.com",
			"isAdmin": true,
			"isActive": true
		}


	]);

// courses
db.courses.insertMany([
		{
			"name": "Professional Development",
			"price": 10000.0
		},
		{
			"name": "Business Processing",
			"price": 13000.0
		}
	])

// find not admin
db.users.find({
	"isAdmin": false
})

// update enrolle
db.courses.updateOne(
		{"name": "Professional Development"},
		{
			$set: {
				"name": "Professional Development",
				"price": 10000.0,
				"enrollees":[
				{
					"userId": ObjectId("620cc81286e8fd109a06e893")
				},
				{
					"userId": ObjectId("620cc81286e8fd109a06e894")
				},
				{
					"userId": ObjectId("620cc81286e8fd109a06e895")
				}]
			}
		}
	)


db.courses.updateOne(
		{"name": "Business Processing"},
		{
			$set: {
				"name": "Business Processing",
				"price": 13000.0,
				"enrollees":[
				{
					"userId": ObjectId("620cc81286e8fd109a06e896")
				},
				{
					"userId": ObjectId("620cc81286e8fd109a06e897")
				},
				{
					"userId": ObjectId("620cc81286e8fd109a06e898")
				}]
			}
		}
	)